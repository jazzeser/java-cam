import java.awt.image.BufferedImage;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.video.BackgroundSubtractorMOG;

public class VideoCap {
	BackgroundSubtractorMOG bs = new BackgroundSubtractorMOG ();
	CascadeClassifier faceDetector = new CascadeClassifier("D:/workspace/Test/bin/lbpcascade_frontalface.xml");
    static{
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    VideoCapture cap;
    Mat2Image mat2Img = new Mat2Image();
    Mat temp = new Mat();
    Mat [] sampleFrames = new Mat[1800];
    Mat [] initFrames = new Mat [27];
    int count =0;
    int width ;
	int height  ;
    
    VideoCap(){
        cap = new VideoCapture();
        cap.open(0);
    } 
    
    BufferedImage getOneFrame() {
    	//temp = new Mat(2,2, CvType.CV_8UC3 ,new Scalar(0,0,255));
        cap.read(temp);
        
        Imgproc.cvtColor(temp,temp,Imgproc.COLOR_RGB2BGR);
        
        if (count ==1)
        {
        	width = initFrames[0].width();
        	height = initFrames[0].height();
        	System.out.println(width +" " + height);
        }
        sampleFrames[count%1800] = temp;
        count++;
      
        
        System.out.println(count);
        return mat2Img.getImage(temp);
        
    }
    BufferedImage getModFrame(){
    	//Mat temp2 = detectImage(temp);
    	if (count < 1800)
    	{
    		for (int i=0; i<27 ;i++)
    		{
    			initFrames [i] = sampleFrames[(int)(Math.random()*count)]; 
    		}
    		return mat2Img.getImage(backgroundModel());
    		
    	}
    	
    	for (int i=0; i<27 ;i++)
		{
			initFrames [i] = sampleFrames[(int)(Math.random()*1800)]; 
		}
    	return mat2Img.getImage(backgroundModel());
    }
    
    /*
     * Default Library background 
     */
    Mat libBack ()
    {
    	
    	Mat tempMat = new Mat();
		tempMat.create(initFrames[0].size(), initFrames[0].type());
		bs.apply(temp, tempMat,0.1);
		return tempMat;
    }
    
    /*
     * face detection (sample)
     */
    Mat detectImage(Mat image)
    {
    	MatOfRect faceDetections = new MatOfRect();
        faceDetector.detectMultiScale(image, faceDetections);

        System.out.println(String.format("Detected %s faces", faceDetections.toArray().length));

        // Draw a bounding box around each face.
        for (Rect rect : faceDetections.toArray()) {
            Core.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
        }
        return image;
    }
    
    /*
     * get background model
     */
    Mat backgroundModel()
    {
    	
    	Mat [] framesRes = new Mat [9];
    	
    	for (int i=0; i<9; i++)
    	{
    		Mat tempMat = new Mat();
    		Mat or12 = new Mat();
    		Mat and3 = new Mat();
    		
    		tempMat.create(initFrames[0].size(), initFrames[0].type());
    		or12.create(initFrames[0].size(), initFrames[0].type());
    		and3.create(initFrames[0].size(), initFrames[0].type());
    		
    		Mat one = initFrames[i*3];
    		Mat two = initFrames[(i*3)+1];
    		Mat three = initFrames[(i*3)+2];
    		
    		
    		Core.bitwise_or(one, two, or12);
    		Core.bitwise_and(or12, three, and3);
    		Core.bitwise_or(or12,and3,tempMat);
    		
    		framesRes [i] = tempMat;
    		
    	}
    	
    	Mat [] framesRes2 = new Mat [3];
    	for (int i=0; i<3; i++)
    	{
    		Mat tempMat = new Mat();
    		Mat or12 = new Mat();
    		Mat and3 = new Mat();
    		
    		tempMat.create(initFrames[0].size(), initFrames[0].type());
    		or12.create(initFrames[0].size(), initFrames[0].type());
    		and3.create(initFrames[0].size(), initFrames[0].type());
    		
    		Mat one = framesRes[i*3];
    		Mat two = framesRes[(i*3)+1];
    		Mat three = framesRes[(i*3)+2];
    		
    		
    		Core.bitwise_or(one, two, or12);
    		Core.bitwise_and(or12, three, and3);
    		Core.bitwise_or(or12,and3,tempMat);
    		
    		framesRes2 [i] = tempMat;
    		
    	}
    	
    	Mat tempMat = new Mat();
		Mat or12 = new Mat();
		Mat and3 = new Mat();
		
		tempMat.create(initFrames[0].size(), initFrames[0].type());
		or12.create(initFrames[0].size(), initFrames[0].type());
		and3.create(initFrames[0].size(), initFrames[0].type());
		
		Mat one = framesRes2[0];
		Mat two = framesRes2[1];
		Mat three = framesRes2[2];
		
		
		Core.bitwise_or(one, two, or12);
		Core.bitwise_and(or12, three, and3);
		Core.bitwise_or(or12,and3,tempMat);
    	
    	return tempMat;
    }
}